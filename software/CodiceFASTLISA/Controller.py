import pandas as pd
import re
import nltk
from  nltk import pos_tag, word_tokenize, sent_tokenize
from transformers import pipeline, AutoTokenizer, AutoModelForSequenceClassification
from Models import *
import translator 
import numpy as np
from sklearn_extra.cluster import KMedoids 
from gensim.models import doc2vec
from sklearn.decomposition import PCA
import torch
from datetime import timedelta
from sentence_transformers import CrossEncoder
from googletrans import Translator
from datetime import date, timedelta

import warnings
warnings.filterwarnings("ignore", category=FutureWarning)

trans = Translator()

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
sim_model = CrossEncoder('efederici/cross-encoder-umberto-stsb')


class languageController:   
    #inserisce settings in un oggetto Lingua   
    def get_lingua(lang):
        if lang == 'it':
            language = Language()
            language.lang = lang
            language.tokenizer= ""
            language.HsModel = ""
            language.SentimentModel = ""
            language.language = "italian"
            language.labelsM = list()
            language.labelsT = list()
            language.hatefulLabel = ""
            language.grammar = """
          N:   {<NN.*>}
          DET: {<DT>}
          NP:  {<DET><N><N>?<PP>?}
                {<DET>?<N><N>*<FW>?<N><N>*}
                {< NNP>}
          punct: {<\.>}
          comma:  {<,>}
          PREM_DET: {<[B|b]ecause>|<[T|t]hus>|<[T|t]herefore><[H|h]ence>|<[T|t]ill>}
          PP:  {<IN><NP>}
          VP:  {<V.*><NP>}
                {<V.*><NP><PP>}
          PREM: {<PRP>?<EX>?<MD><V.*><TO><[A-Z].*>*<punct>}
                {<RB><V.*><[A-Z]*>*<punct>}
                {<TO><V.>*<[A-Z]*>*<comma>?<punct>?}
                {<IN><EX><VBD><[A-Z].*>*<comma><EX><MD><[A-Z].*>*<punct>}
                {<PREM_DET><.*>*}
          CLAIM:
                {<PRP><VBP><IN><VBG><[A-Z].*>*<punct>}
                {<NP><V.*><VP><[A-Z]*>*<comma>?<punct>?}
                {<N|NP><N|NP>?<VBZ><VP>?<[A-Z].*>*<punct><punct>?}
                {<PRP>?<EX>?<MD><V.*><TO><[A-Z].*>*<punct>}
                {<NP><CC>?<NP>?<VBP><VBN><punct>}
                {<PRP|PRP$><A-Z]>*<VB*><TO><[A-Z]>*<comma>?<punct>?}
          S:   {<NP><VP>}

        """
            return language

        elif lang == 'es':
            language = Language()
            language.lang = lang
            language.tokenizer= ""
            language.HsModel = "Hate-speech-CNERG/dehatebert-mono-spanish"
            language.SentimentModel = ""
            language.language = "spanish"
            language.labelsM = ['1 Odio racial/étnico', '2 Ideologías', '3 Negación del genocidio', '4 Odio religioso', '5 Odio de género/orientación sexual']
            language.labelsT = ['Ancianos', 'Cristianos', 'Discapacitados', 'Judíos', 'Etnia', 'Menores', 'Otros grupos religiosos', 'Migrantes', 'Musulmanes', 'Homosexuales', 'Raza', 'Tansgénero', 'Mujer']
            language.hatefulLabel = "HATE"
            language.grammar = """
          N:   {<NN.*>}
          DET: {<DT>}
          NP:  {<DET><N><N>?<PP>?}
                {<DET>?<N><N>*<FW>?<N><N>*}
                {< NNP>}
          punct: {<\.>}
          comma:  {<,>}
          PREM_DET: {<[B|b]ecause>|<[T|t]hus>|<[T|t]herefore><[H|h]ence>|<[T|t]ill>}
          PP:  {<IN><NP>}
          VP:  {<V.*><NP>}
                {<V.*><NP><PP>}
          PREM: {<PRP>?<EX>?<MD><V.*><TO><[A-Z].*>*<punct>}
                {<RB><V.*><[A-Z]*>*<punct>}
                {<TO><V.>*<[A-Z]*>*<comma>?<punct>?}
                {<IN><EX><VBD><[A-Z].*>*<comma><EX><MD><[A-Z].*>*<punct>}
                {<PREM_DET><.*>*}
          CLAIM:
                {<PRP><VBP><IN><VBG><[A-Z].*>*<punct>}
                {<NP><V.*><VP><[A-Z]*>*<comma>?<punct>?}
                {<N|NP><N|NP>?<VBZ><VP>?<[A-Z].*>*<punct><punct>?}
                {<PRP>?<EX>?<MD><V.*><TO><[A-Z].*>*<punct>}
                {<NP><CC>?<NP>?<VBP><VBN><punct>}
                {<PRP|PRP$><A-Z]>*<VB*><TO><[A-Z]>*<comma>?<punct>?}
          S:   {<NP><VP>}

        """
            return language
 
        elif lang == 'ca':
            language = Language()
            language.lang = lang
            language.tokenizer= ""
            language.HsModel = "Hate-speech-CNERG/dehatebert-mono-spanish"
            language.SentimentModel = ""
            language.language = "spanish"
            language.labelsM = ['1 Odio racial/étnico', '2 Ideologías', '3 Negación del genocidio', '4 Odio religioso', '5 Odio de género/orientación sexual']
            language.labelsT = ['Ancianos', 'Cristianos', 'Discapacitados', 'Judíos', 'Etnia', 'Menores', 'Otros grupos religiosos', 'Migrantes', 'Musulmanes', 'Homosexuales', 'Raza', 'Tansgénero', 'Mujer']
            language.hatefulLabel = "HATE"
            language.grammar = """
          N:   {<NN.*>}
          DET: {<DT>}
          NP:  {<DET><N><N>?<PP>?}
                {<DET>?<N><N>*<FW>?<N><N>*}
                {< NNP>}
          punct: {<\.>}
          comma:  {<,>}
          PREM_DET: {<[B|b]ecause>|<[T|t]hus>|<[T|t]herefore><[H|h]ence>|<[T|t]ill>}
          PP:  {<IN><NP>}
          VP:  {<V.*><NP>}
                {<V.*><NP><PP>}
          PREM: {<PRP>?<EX>?<MD><V.*><TO><[A-Z].*>*<punct>}
                {<RB><V.*><[A-Z]*>*<punct>}
                {<TO><V.>*<[A-Z]*>*<comma>?<punct>?}
                {<IN><EX><VBD><[A-Z].*>*<comma><EX><MD><[A-Z].*>*<punct>}
                {<PREM_DET><.*>*}
          CLAIM:
                {<PRP><VBP><IN><VBG><[A-Z].*>*<punct>}
                {<NP><V.*><VP><[A-Z]*>*<comma>?<punct>?}
                {<N|NP><N|NP>?<VBZ><VP>?<[A-Z].*>*<punct><punct>?}
                {<PRP>?<EX>?<MD><V.*><TO><[A-Z].*>*<punct>}
                {<NP><CC>?<NP>?<VBP><VBN><punct>}
                {<PRP|PRP$><A-Z]>*<VB*><TO><[A-Z]>*<comma>?<punct>?}
          S:   {<NP><VP>}

        """
            return language

        elif lang == 'de':
            language = Language()
            language.lang = lang
            language.tokenizer= ""
            language.HsModel = ""
            language.SentimentModel = ""
            language.language = "german"
            language.labelsM = list()
            language.labelsT = list()
            language.hatefulLabel = "HATE"
            language.grammar = """
          N:   {<NN.*>}
          DET: {<DT>}
          NP:  {<DET><N><N>?<PP>?}
                {<DET>?<N><N>*<FW>?<N><N>*}
                {< NNP>}
          punct: {<\.>}
          comma:  {<,>}
          PREM_DET: {<[B|b]ecause>|<[T|t]hus>|<[T|t]herefore><[H|h]ence>|<[T|t]ill>}
          PP:  {<IN><NP>}
          VP:  {<V.*><NP>}
                {<V.*><NP><PP>}
          PREM: {<PRP>?<EX>?<MD><V.*><TO><[A-Z].*>*<punct>}
                {<RB><V.*><[A-Z]*>*<punct>}
                {<TO><V.>*<[A-Z]*>*<comma>?<punct>?}
                {<IN><EX><VBD><[A-Z].*>*<comma><EX><MD><[A-Z].*>*<punct>}
                {<PREM_DET><.*>*}
          CLAIM:
                {<PRP><VBP><IN><VBG><[A-Z].*>*<punct>}
                {<NP><V.*><VP><[A-Z]*>*<comma>?<punct>?}
                {<N|NP><N|NP>?<VBZ><VP>?<[A-Z].*>*<punct><punct>?}
                {<PRP>?<EX>?<MD><V.*><TO><[A-Z].*>*<punct>}
                {<NP><CC>?<NP>?<VBP><VBN><punct>}
                {<PRP|PRP$><A-Z]>*<VB*><TO><[A-Z]>*<comma>?<punct>?}
          S:   {<NP><VP>}

        """
            return language

class controllerGetComments:
    #restituisce una nuova lista di commenti puliti da inserire nel db. 
    def clean_sentences(df):
        clean_sentences = list()
        for comment in list(df['comment_content']):
            comment = comment.replace(r'\.',r'\.\s')
            comment = re.sub('\n',' ', comment)
            comment = comment.replace('\r',' ')
            comment = re.sub(r'^\t+', ' ', comment)
            comment = re.sub(r"\<\/?\w*\>", "", comment)
            comment = comment.replace(' \W*', '')
            comment = comment.replace('·', '')
            comment = comment.replace('-', '')
            comment = comment.replace('&nbsp;', ' ')
            comment = re.sub(r'\s+', ' ', comment)
            comment = re.sub(r'^\s*', '', comment)
            clean_sentences.append(comment)
        return clean_sentences  

class controllerPredictions:
    # crea oggetto predicted comment 
    def get_prediction( comment, language, Hs_classifier, inference_classifier):

        predicted_comment = PredictedComment(comment) 
        #do HS
        Hs_prediction = Hs_classifier(comment)
        predicted_comment.Hs = Hs_prediction[0]['label'] 
        if predicted_comment.Hs  == language.hatefulLabel:
            #do ASCENT
            #ASCENT Motivations
            Motivation_prediction = inference_classifier(comment, language.labelsM, multi_label=True)
            predicted_comment.Motivation = [Motivation_prediction['labels'][0], Motivation_prediction['labels'][1]]
            #ASCENT Targets
            Target_prediction = inference_classifier(comment, language.labelsT, multi_label=True)
            
            predicted_comment.Target = [Target_prediction['labels'][0], Target_prediction['labels'][1]]
        else:
            predicted_comment.Motivation = 'no label'
        return predicted_comment

class controllerMostCentralItem:
    def get_most_central(itemslist):
        def do_d2v(my_list):
            #DOC2VEC
            
            if (type(my_list[0]) is str):
                claims = [x.split(' ') for x in my_list]
            else :
                claims = [x.text.split(' ') for x in my_list]       
            documents = [doc2vec.TaggedDocument(doc, [i]) for i, doc in enumerate(claims)]
            model = doc2vec.Doc2Vec(documents, vector_size=5, window=2, min_count=1, workers=4)
            vecs = [model.infer_vector(i) for i in claims]
            pca = PCA(n_components=1).fit(vecs)
            vecs = pca.transform(vecs)
            return vecs


        vectors = do_d2v(itemslist)
        items = [x for x in itemslist]
        kmedo = KMedoids(n_clusters = 1, metric='cosine', random_state=1, method='pam', init='k-medoids++')
        X = np.array(vectors)
        X = X.reshape(-1, 1)
        kmedo.fit(X)
        #Adding predicted labels to the original data
        centroids = np.array(kmedo.cluster_centers_)
        #first level 
        # main_Claim = df[df['vectors']== centroids[0][0]]['comment_content'].values[0]
        MostCentralItem = items[list(vectors).index(centroids[0][0])]

        return MostCentralItem

class controllerMostCentralItemRelations:
    def get_relations(item, candidateslist, language):
        tokenizer = AutoTokenizer.from_pretrained(language.inference_model)
        inference_model = AutoModelForSequenceClassification.from_pretrained(language.inference_model)
        results = []
        premise = item
        for sentence in list(candidateslist):
            hypothesis = sentence

            input = tokenizer(str(premise), str(hypothesis), truncation=True, return_tensors="pt")
            output = inference_model(input["input_ids"].to(device))  # device = "cuda:0" or "cpu"
            prediction = torch.softmax(output["logits"][0], -1).tolist()
            label_names = ["entailment", "contradiction"]
            prediction = {name: round(float(pred) * 100, 1) for pred, name in zip(prediction, label_names)}
            
            if prediction['entailment'] > prediction['contradiction']:
                pred = 'pro'
            else:
                pred = 'con' 
            results.append(pred)
        return results

class get_C_P:
    def __init__(self, comments, language):
        claims_list = list()
        prems_list = list()
        claims = list()
        prems = list()
        for sent in list(comments['comment_content']):
            
            doc = trans.translate(str(sent), dest='en')
            doc.text = doc.text.replace(".", ". ")

            document = doc.text
            tokens_tag = pos_tag(word_tokenize(document, language=language.language))
            # print("tokens =", tokens_tag)
            grammar = """
          NP:  {<DT><NN.*><NN.*>?<PP>?}
                {<DT>?<NN.*><NN.*>*<FW>?<NN.*><NN.*>*}
                {< NNP>}
          punct: {<\.>}
          comma:  {<,>}
          PREM_DET: {"because"|"thus"|"therefore"|"hence"|"till"|"that is"}
          PP:  {<IN><NP>}
          VP:  {<V.*><NP>}
                {<V.*><NP><PP>}
          PREM: 
                {<[A-Z]*>*<PREM_DET><[A-Z]*>*<comma>?<punct>?}
                {<[A-Z]*>*<WDT><[A-Z]*>*<punct>?}
                {<[A-Z]*>*<MD><[A-Z]*>*<punct>?}
                
          CLAIM: {<NP><V.*><VP><[A-Z]*>*<comma>?<punct>?}
                {<PRP><VB.*><NN.*><VP><[A-Z]*>*<comma>?<punct>?}
                {<DT>?<NN.*><VB.*>*<DT><NN.*><punct>?}
                {<[A-Z]*>*<NN.*><VB.*><IN>*<DT>*<JJ><[A-Z]*>*<comma>?<punct>?}

                {<EXT><VBZ><[A-Z]*>*<punct>?}


                {<DT><N>}
                {<PRP><VBP><IN><VBG><[A-Z].*>*<punct>}

            S:  {<NP><VP>}

        """
            # check if there are 
            cp = nltk.RegexpParser(grammar)
            tree = cp.parse(tokens_tag)

            for subtree in tree.subtrees(filter=lambda t: t.label() == 'CLAIM'):
                print(subtree)
                claims_list.append(subtree)
                new_sent = list()
                for text, item in subtree.leaves():
                    new_sent.append(text)
                new_sent = ' '.join(new_sent)
                my_claim = Arg_node('Claim', 'Claim', 'neutral',new_sent, trans.translate(new_sent, src="en",dest=language.lang).text)
                claims.append(my_claim)
                print(len(claims), "---> len my claim")


            for subtree in tree.subtrees(filter=lambda t: t.label() == 'PREM'):

                prems_list.append(subtree)
                new_sent = list()
                for text, item in subtree.leaves():
                    new_sent.append(text)
                new_sent = ' '.join(new_sent)
                my_prem = Arg_node('Prem', 'Premise', 'neutral',new_sent, trans.translate(new_sent, src="en",dest=language.lang).text)
                prems.append(my_prem)

        print("TOT CLAIMS =",len(claims))
        print("TOT PREMISES =",len(prems))
        
        self.get_claims = claims
        self.get_prems = prems


class get_Rel:
  def __init__(self, parent, children_list, language):
    inference_model = AutoModelForSequenceClassification.from_pretrained(language.inference_model)
    tokenizer = AutoTokenizer.from_pretrained(language.inference_model)
    results = []
    children = [x.text for x in children_list]
    for sentence in children:
          scores = sim_model.predict([(parent, sentence)])
          results.append([parent, sentence, scores])
    
    results_df=pd.DataFrame(results).sort_values(by=[2], ascending=False)
    child= list(results_df[results_df.columns[1]])[0]
 

    self.child = child
    
    premise = str(child)
    hypothesis = str(parent)
    input = tokenizer(premise, hypothesis, truncation=True, return_tensors="pt")
    output = inference_model(input["input_ids"].to(device))  # device = "cuda:0" or "cpu"
    prediction = torch.softmax(output["logits"][0], -1).tolist()
    label_names = ["entailment", "contradiction"]
    prediction = {name: round(float(pred) * 100, 1) for pred, name in zip(prediction, label_names)}
    
    if prediction['entailment'] > prediction['contradiction']:
      pred = 'pro'
    else:
      pred = 'con' 
    self.rel_pred = pred

class controllerArgMining:

    def get_arg_items(comments, language):
        
        shade_pro = '#42CA86'
        shade_con = '#5A4CEF'

      
        # GET claim and prem 
        claims_prems = get_C_P(comments, language)
        claims = claims_prems.get_claims
        premises = claims_prems.get_prems

        main_Claim = controllerMostCentralItem.get_most_central(claims)
        for item in claims:
            if item.text == main_Claim.text:
                item.name = "Main Claim"
        
        claims_done = 0
        nodes = list()
        relations = list()

        b = [e for e in claims if e.name == "Main Claim"][0]
        children =  [e for e in claims if e.name != "Main Claim"]
        parent = main_Claim
        while claims_done <= 4: 
            if claims_done ==0:
                node = dict()
                node['id']="Main Claim"
                node['title'] = b.textEs
                node['color'] = 'white'
                node['borderColor'] = 'black'
                node['dataLabels'] = {"color": "black"}
                nodes.append(node)
                claims_done+=1
            else:
                mainC_re = get_Rel(parent.text, children, language)
                parent_node =  [e for e in claims if e.text == str(parent.text)][0]
                parent_op = parent_node.opinion
                child = mainC_re.child
                pred = mainC_re.rel_pred
                print('-------\n',parent_node.name,'\n text =',parent_node.text, "\n PREDICTION =", pred, '\n===============')

                for item in claims:
                    if item.text == child :
                        item.name = "Claim"+str(claims_done)
                        text = item.textEs


                node = dict()
                node['id']="Claim"+str(claims_done)
                node['title'] = text
                node['color'] = 'white'
                rel_node = [ parent_node.name, "Claim"+str(claims_done), pred]
                if parent_op == 'neutral':
                    if pred == 'con':
                        rel_node.append(shade_con)
                        node['borderColor'] = shade_con
                        for item in claims:
                            if item.text == child:
                                item.opinion = pred
                    if pred == 'pro':
                        rel_node.append(shade_pro)
                        node['borderColor'] = shade_pro
                        for item in claims:
                            if item.text == child:
                                item.opinion = pred
                if parent_op == 'pro':
                    if pred == 'con':
                        rel_node.append(shade_con)
                        node['borderColor'] = shade_con
                        for item in claims:
                            if item.text == child:
                                item.opinion = pred
                    if pred == 'pro':
                        rel_node.append(shade_pro)
                        node['borderColor'] = shade_pro
                        for item in claims:
                            if item.text == child:
                                item.opinion = pred
                if parent_op == 'con':
                    if pred == 'con':
                        rel_node.append(shade_con)
                        node['borderColor'] = shade_pro
                        for item in claims:
                            if item.text == child:
                                item.opinion = 'pro'
                    if pred == 'pro':
                        rel_node.append(shade_con)
                        node['borderColor'] = shade_con
                        for item in claims:
                            if item.text == child:
                                item.opinion = 'con'

                node['dataLabels'] = {"color": "black"}
                nodes.append(node)
                relations.append(rel_node)

                parent = [e for e in claims if e.text == child][0]
                children =  [e for e in children if e.text != str(child)]
                claims_done+=1

        claims = [e for e in claims if e.name != "Claim"]

        print('LEN PREMS = ', len(premises))
        print('LEN CLAIMS = ', len(claims))
        parent = b.text
        all_args = 1

        for claim in claims:
            parent = claim
            parent_name = parent.name
            print(parent_name)
            claims_done=0
            while claims_done <2: 
                mainC_re = get_Rel(parent.text, premises, language)
                parent_node =  [e for e in claims if e.text == str(parent.text)][0]
                parent_op = claim.opinion
                child = mainC_re.child
                pred2 = mainC_re.rel_pred
                print('Parent =', parent_name, "\n Arg",claims_done,'\n text =', child )
                b = [e for e in premises if e.text == child ][0] 

                node = dict()
                node['id']=parent_name+"_arg"+str(claims_done+1)
                node['title'] = b.textEs
                rel_node = [ parent_node.name, parent_name+"_arg"+str(claims_done+1), pred2]
                if parent_op == 'neutral':
                    if pred == 'con':
                        rel_node.append(shade_con)
                        node['name'] = '#con'
                        node['color'] = shade_con
                        for item in premises:
                            if item.text == child:
                                item.opinion = pred
                                item.name = parent_name+"_arg"+str(claims_done+1)
                    if pred == 'pro':
                        rel_node.append(shade_pro)
                        node['name'] = '#pro'
                        node['color'] = shade_pro
                        for item in premises:
                            if item.text == child:
                                item.opinion = pred
                                item.name = parent_name+"_arg"+str(claims_done+1)
                if parent_op == 'pro':
                    if pred == 'con':
                        node['name'] = '#con'
                        rel_node.append(shade_con)
                        node['color'] = shade_con
                        for item in premises:
                            if item.text == child:
                                item.opinion = pred
                                item.name = parent_name+"_arg"+str(claims_done+1)
                    if pred == 'pro':
                        node['name'] = '#pro'
                        rel_node.append(shade_pro)
                        node['color'] = shade_pro
                        for item in premises:
                            if item.text == child:
                                item.opinion = pred
                                item.name = parent_name+"_arg"+str(claims_done+1)
                if parent_op == 'con':
                    if pred == 'con':
                        node['name'] = '#pro'
                        rel_node.append(shade_con)
                        node['color'] = shade_pro
                        for item in premises:
                            if item.text == child:
                                item.opinion = 'pro'
                                item.name =parent_name+"_arg"+str(claims_done+1)
                    if pred == 'pro':
                        node['name'] = '#con'
                        rel_node.append(shade_pro)
                        node['color'] = shade_con
                        for item in premises:
                            if item.text == child:
                                item.opinion = 'con'
                                item.name = parent_name+"_arg"+str(claims_done+1)
                nodes.append(node)
                
                relations.append(rel_node)

                premises =  [e for e in premises if e.name != parent_name+"_arg"+str(claims_done+1)]
                all_args+=1
                claims_done+=1

        return relations, nodes
class controllerVisualizations:
    def do_pie(comments, label):
        countHS = len(comments[comments["HS"] == label])
        countNoHS = len(comments[comments["HS"] != label])
        pie_list = [{ "name": "Hate Speech", "y": countHS},{"name": "Normal language","y": countNoHS}]

        return pie_list
    
    def do_barchart(comments, listTarget):
        barchart_list = list()
        for item in listTarget:
            subset_Target = comments[comments['Target'].astype(str).str.contains(item)]
            # subset_Target = comments[comments['Target'].apply(lambda x: item in x)]
            if len(subset_Target) > 0 :
                count_items = len(subset_Target)
                node_Target = {"name": item,"y": count_items}
                barchart_list.append(node_Target)
          
        return barchart_list
    
    def do_heatmap(comments, label):
        comments = comments[comments['HS']==label]
        comments['Motivation_str'] = comments['Motivation'].apply(lambda x: ', '.join(x))
        comments_expl = comments.explode('Motivation')
        comments_expl = comments_expl[comments_expl['Target']!='no label']
        categories = list(set(comments.explode('Motivation')['Motivation']))
        categories_map = dict()
        cat_n = 0
        for cat in list(categories):
            categories_map[cat]=cat_n            
            cat_n+=1
        data_hm = list()
        for cat1 in list(categories):
            cat1_n = categories_map[cat1]
            for cat2 in list(categories):
                cat2_n = categories_map[cat2]
                cats = [cat1,cat2]
                support = [cat1_n,cat2_n]
                my_str = ', '.join([cat1, cat2])
                count = len(comments[comments['Motivation_str']== my_str])
                support.append(count)
                data_hm.append(support) 
        list_heatmap = [{ "categories": list(categories)},{"data": data_hm}]
        
        return list_heatmap
          
    def do_line_chart(df, label):
        def do_range_dates(df):
            dates_list = pd.Series(list(df['comment_date_gmt'])).drop_duplicates().tolist()
            start_date = dates_list[0].split('-')
            end_date = dates_list[-1].split('-')
            start_date = date(int(start_date[0]),int(start_date[1]),int(start_date[2]))
            end_date = date(int(end_date[0]),int(end_date[1]),int(end_date[2]),)
            # difference between current and previous date
            delta = timedelta(days=1)
            # store the dates between two dates in a list
            dates = []
            while start_date <= end_date:
                # add current date to list by converting  it to iso format
                dates.append(str(start_date))
                # increment start date by timedelta
                start_date += delta
            return dates
        line_df = df.copy().sort_values(by=['comment_date_gmt'])
        line_df['comment_date_gmt'] =line_df['comment_date_gmt'].apply(lambda x: x[:-9])
        dates = do_range_dates(line_df)
        data = [{"name": "H.S. comments","color": "#42CA87", "data":[]}, {"name": "N.L. comments","color": "#5a4def", "data":[]}]
        data_HS = dict()
        data_no_HS = dict()
        likes_HS = list()
        likes_no_HS = list()
        tot_HS = list()
        tot_no_HS = list()
        rep_HS = list()
        rep_no_HS = list()
        for my_date in dates:
            temp= line_df[line_df['comment_date_gmt'] == my_date]
            temp_HS = temp[temp['HS']== label]
            temp_no_HS = temp[temp['HS']!= label]
            
            count_HS = len(temp_HS)
            tot_HS.append(int(count_HS))
            count_no_HS = len(temp_no_HS)
            tot_no_HS.append(int(count_no_HS))
            
            like_HS = temp_HS['meta:wpdiscuz_votes'].apply(lambda x: int(x.replace("'",'') if type(x) == str else int('0') if type(x) == float else x)).sum()
            likes_HS.append(int(like_HS))
            like_no_HS = temp_no_HS['meta:wpdiscuz_votes'].apply(lambda x: int(x.replace("'",'') if type(x) == str else int('0') if type(x) == float else x)).sum()
            likes_no_HS.append(int(like_no_HS))
            
            id_HS = temp_HS.comment_ID
            r_HS = 0
            if len(id_HS)>0:
                for id in id_HS:
                    r_HS += len(temp_HS[temp_HS['comment_parent']== id])
            rep_HS.append(int(r_HS))

            id_no_HS = temp_no_HS.comment_ID
            r_no_HS = 0
            if len(id_no_HS)>0:
                for id in id_no_HS:
                    r_no_HS += len(temp_no_HS[temp_no_HS['comment_parent']== id])
            rep_no_HS.append(int(r_no_HS))

        data_HS['name']= 'H.S. comments'
        data_HS['data']= tot_HS
        data_HS['likes']= likes_HS
        data_HS['replies']= rep_HS
        data_HS['color']= '#42CA87'

        data_no_HS['name']= 'N.L. comments'
        data_no_HS['data']= tot_no_HS
        data_no_HS['likes']= likes_no_HS
        data_no_HS['replies']= rep_no_HS
        data_no_HS['color']= '#5a4def'

        data = [data_HS, data_no_HS]

        list_line_chart =  [{ "categories": dates},{ "data": data}]
        return list_line_chart
    
    def do_tree(df):
        tree_df = df[df['Motivation']!='no label']
        motivations = tree_df.explode('Motivation')
        motivations_l = list(set(motivations['Motivation']))
        data = dict()
        data['groups']=list()
        for motivation in motivations_l:
            targets = motivations[motivations['Motivation']==motivation].explode('Target')
            targets_l= list(set(targets['Target']))
            sub_groups = list()
            for target in targets_l:
                group = {'label':target, 'weight':len(targets[targets['Target']==target])}
                sub_groups.append(group)
            data['groups'].append({'label': motivation, 'weight': len(motivations[motivations['Motivation']==motivation]), 'groups':sub_groups })
        list_treemap = [data]
        return list_treemap

    def do_sankey(df, label):
        data = list()
        sunk_df = df[df['Motivation']!='no label']
        classes = set(sunk_df.explode('Motivation')['Motivation'])
        sunk_df_HS= sunk_df[sunk_df['HS']==label]
        sunk_df_no_HS= df[df['HS'] !=label]

        count_pro_HS = len(sunk_df_HS[sunk_df_HS['central_comment_relation']=='pro'])
        data.append(['Pro', 'HS', count_pro_HS])
        count_contro_HS = len(sunk_df_HS[sunk_df_HS['central_comment_relation']=='con'])
        data.append(['Contro', 'HS', count_contro_HS])

        count_pro_no_HS = len(sunk_df_no_HS[sunk_df_no_HS['central_comment_relation']=='pro'])
        data.append(['Pro', 'NO HS', count_pro_no_HS])
        count_contro_no_HS = len(sunk_df_no_HS[sunk_df_no_HS['central_comment_relation']=='con'])
        data.append(['Contro', 'NO HS', count_contro_no_HS])
        temp=pd.DataFrame()
        temp[['mot1','mot2']] = pd.DataFrame(sunk_df_HS.Motivation.tolist(), index= sunk_df_HS.index)
        for c in classes:
            data.append(['HS', c,len(temp[temp['mot1']==c])])
        list_sankey = [{ "data": data}]

        return list_sankey