
class Language:
    def __init__(self):
        self.lang = ""
        self.tokenizer= ""
        self.HsModel = ""
        self.SentimentModel = ""
        self.language = ""
        self.labelsM = list()
        self.labelsT = list()
        self.inference_model = "MoritzLaurer/mDeBERTa-v3-base-xnli-multilingual-nli-2mil7"
        self.hatefulLabel = ""
        self.grammar = ""

class PredictedComment:
    def __init__(self, comment):
        self.comment= comment
        self.Hs = ""
        self.Sentiment = ""
        self.Motivation = list()
        self.Target = list()
        self.inference_model = "MoritzLaurer/mDeBERTa-v3-base-xnli-multilingual-nli-2mil7"


class Arg_node:
  def __init__(self, id, self_type, opinion,text, text_se):
    self.name = id
    self.type = self_type
    self.opinion = opinion
    self.text = text
    self.textEs = text_se

class Claim_node:
  def __init__(self, id, text, color):
    self.id = id
    self.title = text
    self.color = color
    self.borderColor = 'black'
    self.dataLabels = {
                        "color": "black"
                    }


class Prem_node:
  def __init__(self, id, opinion, text, color,):
    self.id = id
    self.name = opinion
    self.title = text
    self.color = color



