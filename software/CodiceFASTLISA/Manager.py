
from Controller import *
import pandas as pd
import re
import nltk
from transformers import pipeline
import tqdm

import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=Warning)

# classe che sceglie i modelli specifici per lingua
class managerLingua:
    def get_lingua(lang):
        lingua = languageController.get_lingua(lang)
        return lingua
# classe che prende i commenti li pulise, li tokkenizza in 
class GetComments():
    def get_comments(path):
        df = pd.read_csv(path, delimiter=';')
        clean_sentences = controllerGetComments.clean_sentences(df)
        df_clean = df.rename(columns={"comment_content": "original_content"})
        df_clean['comment_content'] = clean_sentences
        return df_clean
    
class ClassificationManager():
    #classe che ritorna un nuovo df con aggiunte le colonne HS, Target, Motivation, Most central comment, 
    # relations to central comment, claims nodes, premises nodes
    def __init__(self,comments, wk_language):
        Hs = list()
        Target = list()
        Motivation = list()


        print('.....load classifiers.....')
        Hs_classifier = pipeline("text-classification", model=wk_language.HsModel)
        inference_classifier = pipeline("zero-shot-classification", model=wk_language.inference_model) #mettere dentro qualche altra cosa   
        print('.....classifiers loaded.....')
        print("START classification")
        for comment in list(comments['comment_content']):
            predicted_comment = controllerPredictions.get_prediction(comment, wk_language, Hs_classifier,inference_classifier)
            #do HS
            Hs.append(predicted_comment.Hs)
            Target.append(predicted_comment.Target)
            Motivation.append(predicted_comment.Motivation)

        print("START classification")

        comments['HS'] =Hs
        comments['Target'] =Target
        comments['Motivation'] = Motivation
        
        print('Tot comments with hs =',len(comments[comments['HS']== wk_language.hatefulLabel]))

        # returns a list which indicates which is the most central comment among all
        CentralComment = controllerMostCentralItem.get_most_central(list(comments['comment_content'])) 
        comments['central_comment']= "N"
        #fai regola per cambiare quel testo in "Y"
        centralC = comments[comments['central_comment']== "Y"]
        print("Most central comment for sankey =", centralC)
        #returns a list with relations with most central comment pro (entailment) or con (contraddiction) 
        CentralCommentRelations = controllerMostCentralItemRelations.get_relations(centralC, comments['comment_content'], wk_language) 
        comments['central_comment_relation'] = CentralCommentRelations
        self.classification_df = comments
        #do arguments
        self.nodes, self.relations = controllerArgMining.get_arg_items(comments, wk_language)
 
class VisualizationsManager():
    #classe che ritorna un nuovo df con aggiunte le colonne HS, Target, Motivation, Most central comment, 
    # relations to central comment, claims nodes, premises nodes
    def get_json(classifiedComments, wk_language):
        final_dict = dict()
        #pie-chart HS rate
        print("START ELABORATING PIE")
        final_dict['pie_chart']= controllerVisualizations.do_pie(classifiedComments.classification_df, wk_language.hatefulLabel)
        
        #barchart HS targets 
        print("START ELABORATING BAR")
        final_dict['bar_chart']= controllerVisualizations.do_barchart(classifiedComments.classification_df, wk_language.labelsT)

        #heatmap
        print("START ELABORATING HEAT")
        final_dict['heat_map'] = controllerVisualizations.do_heatmap(classifiedComments.classification_df, wk_language.hatefulLabel)

        #line-chart
        print("START ELABORATING LINE")
        final_dict['line_chart']= controllerVisualizations.do_line_chart(classifiedComments.classification_df, wk_language.hatefulLabel)

        #tree
        print("START ELABORATING TREE MAP")
        final_dict['tree_map'] = controllerVisualizations.do_tree(classifiedComments.classification_df)
    
        #sankey
        print("START ELABORATING SANKEY")
        final_dict['sankey_diagram'] = controllerVisualizations.do_sankey(classifiedComments.classification_df, wk_language.hatefulLabel)

        #argMap
        print("START ELABORATING ARGUMENTS")
        final_dict['argument_map']=[{'data':classifiedComments.nodes},{'nodes':classifiedComments.relations}]

        
      
        
        return final_dict

