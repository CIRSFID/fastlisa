# FASTLISA CLASSIFIER



## Getting started

Follow this procedure the install a new virtual environment for the Classifier and project's dependencies

1. Download the Codice FAST LISA from Git Hub
2. Open Terminal at the downloaded folderFolder
3. Execute the following commands

```
pip3 install virtualenv 
```
```
virtualenv env_name --python=python3.9.6
```
```
source env_name/bin/activate    
```
```
pip3 install -r requirements.txt        

```

#### SET UP is complete you can now run the program

### How to run it:

1. Open terminal at FOlder containing the file Program.py
2. Execute the following commands using the name of the previously created environment:
```
source env_name/bin/activate    
```
```
python -c 'from Program  import Program;Program("/your/path/to/file.csv", "ca")'
```
Available languages as input:
- Geman: "de"  
- Italian: "it"
- Spanish: "es"
- Catalan: "ca"

### Output:

At the end of each elaboration, results will be available in the output folder:

- a .json file of resulting visualizations to upload on the dashboard.
- a .txt file containing further information to add in the dashboard's web page.

