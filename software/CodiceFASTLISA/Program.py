
from Manager import *
from os.path import join 
import json
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)

class Program:
    def __init__(self, path, lang):        

        print('START PROGRAM')
        print("Prova PATH:"+ path[:-4])
        
        #crea oggetto lingua che cintiene i nomi dei modelli specifici per lingua    

        print('Enter managerLingua')
        wk_language = managerLingua.get_lingua(lang)
        print('Exit managerLingua')
        
        print(wk_language.hatefulLabel)
        #modello generale per tutte le lingue

        print('Enter GetComments')
        comments= GetComments.get_comments(path) #returns df with cleaned and tokenized comments and another with metadata for further elaboration
        print('Exit GetComments')

        #count comments 
        commentsCount = len(comments)

        #do classification 
        print('Enter ClassificationManager')
        classifiedComments = ClassificationManager(comments,wk_language)
        print('Exit ClassificationManager')
        
        #do visualizations
        print('Enter VisualizationsManager')
        path = path.split("/")
        final_json = VisualizationsManager.get_json(classifiedComments,wk_language)
        print('Exit ClassificationManager')
        
        #save final txt and  json file
        engag_rate = (len(comments[comments['comment_parent']!=0].groupby(['comment_author_IP']).count())/len(comments.groupby(['comment_author_IP']).count()))*100
        
        print
        with  open(join("output",path[-1][:-4]+".txt"), "w") as f:
         f.write('FURTHER INFORMATIONS TO ADD IN THE BASHBOARD')
         f.write('Total comments = ', len(comments))
         f.write('Discussion period = ', len(comments.groupby(['comment_date']).count()))
         f.write('Total participants = ', len(comments.groupby(['comment_author_IP']).count()))
         f.write('Engagemnt rate= '+str(engag_rate)+'%')


        with open(join("output",path[-1][:-4]+".json"), "w") as outfile:
            json.dump(final_json, outfile, indent = 4)
        