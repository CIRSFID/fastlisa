$(document).ready(function() {

    $('#sidebar_btn').click(function(){
        $('#my-awesome-nav').parent().toggleClass("is-hidden");
        $("#show-sidebar").toggleClass("is-hidden");
        $("#hide-sidebar").toggleClass("is-hidden");
        if ($(".grid").hasClass("open")) { 
            $(".grid").css({"grid-template-columns":"100%"});
            $(".grid").toggleClass("open");
            $("#sidebar_btn").css({"left":"0%"});
        } else { 
            var x = window.matchMedia("(max-width: 700px)")

            if (x.matches){
                $(".grid").css({"grid-template-columns":"20% 80%"});
            $(".grid").toggleClass("open");
            $("#sidebar_btn").css({"left":"20%"});

            }else{
            $(".grid").css({"grid-template-columns":"15% 85%"});
            $(".grid").toggleClass("open");
            $("#sidebar_btn").css({"left":"15%"});
        }
        } 

      });

    var spy = new Gumshoe('#my-awesome-nav a', {
        nested: true,
    });
    


    // $(".my-awesome-nav").css({
    //     'width': ($(".reference-nav").width() + 'px')
    //   });

    // top button in index
    $(".to-top").click(function(){
    $(".nav-index").animate({ scrollTop: 0 }, "slow");
    });

    $(".expand").click(function(){
        $('.index-item').removeClass('is-hidden');
        $(".fa-solid.fa-chevron-up").removeClass("is-hidden");
        $(".fa-solid.fa-chevron-right").addClass("is-hidden");
        $(".collapse").toggleClass("is-hidden");
        $(".expand").toggleClass("is-hidden");
    });

    $(".collapse").click(function(){
        $('.index-item').addClass('is-hidden');
        $(".fa-solid.fa-chevron-up").addClass("is-hidden");
        $(".fa-solid.fa-chevron-right").removeClass("is-hidden");
        $(".expand").toggleClass("is-hidden");
        $(".collapse").toggleClass("is-hidden");
        
          
    });

    // $(".scroll-current").click(function(){

    //     $('.index-item').removeClass('is-hidden');
    //     $(".fa-solid.fa-chevron-up").removeClass("is-hidden");
    //     $(".fa-solid.fa-chevron-right").addClass("is-hidden");
    
    //     scroller = "#idx-"+$($('li.active').last().find('*').get(0)).attr('href').slice(1,);
    //     console.log($(scroller)); 
    //     num = $(scroller).offset().top;
    //     console.log(num); 
    //     $(".nav-index").animate({ scrollTop:num}, "slow");
  
  
    // });

    // hide display buttons in index 

    $('[class^=navLink]').click(function() {
    
        toggleHidden(this);
      });
      
      function toggleHidden(ele) {
        console.log(ele)
        $(ele).children().toggleClass("is-hidden");
        $(ele).siblings("ul").toggleClass("is-hidden");
      }

});

